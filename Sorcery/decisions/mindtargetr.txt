targetted_decisions = {
	1mind_target_decision = {
		ai_target_filter   = all
		filter = all
		from_potential = {
			is_playable = yes
			ai = no
			OR = {
				trait = Sorcery 
				trait = Mentalist
			}
		}
	
		potential = {
			NOT = { has_character_modifier = magic_shield }
		}
		allow = { 		
		}
		
		effect = {	
			FROM = {
				opinion = {
					who = ROOT
					modifier = mindmanip
					months = 12000
				}
			}		
		}
		revoke_allowed = {
			always = no
		}
		ai_will_do = {
			factor = 1
		}
	}	
}