targetted_decisions = {
	transmute_body_decision = {
			
		ai_target_filter   = all
		filter = all
		from_potential = {
		wealth = 500
			is_playable = yes
			ai = no			
			NOT = {	OR= { trait = incapable has_character_flag = sorcery}} 
			trait = Alchemist
		}
		potential = {
		OR = {
			NOT = { has_character_modifier = magic_shield }
		}
		}
		
		allow = {  
			ROOT = { at_location = FROM } 										
		}
		effect = {
			save_event_target_as = transmutebodytarget			
			FROM = {
			character_event = {id = transmutebody.1 }	
			}	
		}
		revoke_allowed = {
			always = no
		}
		ai_will_do = {
			factor = 1
		}
	}
	}